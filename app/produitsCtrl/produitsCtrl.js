(function() {
    angular.module('app').controller('produitsCtrl', ['$scope', 'produitSrvc', function($scope, produitSrvc) {

        this.listeProduits = produitSrvc.listeProduits;
        produitSrvc.loadProduitAsync();

    }]);
})();