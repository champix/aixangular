(function() {
    'use strict';

    angular
        .module('app')
        .controller('produitCtrl', ControllerController);

    ControllerController.$inject = ['$scope', '$routeParams', 'produitSrvc'];

    function ControllerController($scope, $routeParams, produitSrvc) {
        var vm = this;
        var _id = $routeParams.idp;
        this.p = produitSrvc.listeProduits;
        produitSrvc.loadProduitAsync().then(function(r) {
            vm.produit =
                produitSrvc.getProduit(_id);
        });

    }
})();