(function() {
    'use strict';

    angular
        .module('app')
        .filter('currencyPerso', function() {
            function currencyPersoFilter(value, currencyChar, precision) {
                var ret = '';
                var round = Math.round(value * Math.pow(10, precision)) / Math.pow(10, precision);

                ret = ret + round + ' ' + currencyChar;
                return ret;
            }

            return currencyPersoFilter;
        })
        .filter('reduction', function() {
            function reductionFilter(value, pcent) {
                var pc = (pcent !== undefined ? pcent : 20);
                return value - value * (pc / 100);
            }
            return reductionFilter;
        });
})();