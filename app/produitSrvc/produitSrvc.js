(function() {
    'use strict';

    angular
        .module('app')
        .service('produitSrvc', ['$http', '$q', function($http, $q) {
            var vm = this;
            this.listeProduits = [];
            var _isloaded = false,
                _endload = false;
            var _loadProduits = function() {
                _isloaded = true;
                return $http.get('http://localhost:775/produit')
                    .then(function(response) {
                        _endload = true;
                        response.data.map(function(unElement) {
                            vm.listeProduits.push(unElement);
                        });
                    }, function(response) {
                        console.log('hello', response);
                    });
            };

            this.getProduit = function(id) {

                for (var i = 0; vm.listeProduits.length > i; i++) {

                    if (vm.listeProduits[i].id == id)
                        return vm.listeProduits[i];

                }
                return null;

            };
            this.loadProduitAsync = function() {

                if (!_isloaded) return _loadProduits();
                var e = _endload;
                // perform some asynchronous operation, resolve or reject the promise when appropriate.
                return $q(function(resolve, reject) {
                    //var _vm_vm=vm;
                    setTimeout(function(params) {
                        while (!e) setTimeout('', 10);
                    }, 1000);
                    if (!e) reject();
                    else resolve();
                });
            };
        }]);
})();