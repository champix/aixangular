(function() {
    ////contenu de la page js encapsulé 
    var app = angular.module('app', ['ngRoute']).config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/accueil', { template: '<h2>Bienvenue sur le site de E-boutique </h2><p>ce site est un TP pour la societe orsys, dedie a la techno JS angular ecrite par google</p><hr/>' })
            .when('/produits', { templateUrl: 'vues/produits.html' })
            .when('/produit/:idp', {
                templateUrl: 'vues/produit.html',
                controller: 'produitCtrl',
                controllerAs: 'pctrl',

            })
            .otherwise({ redirectTo: '/accueil' })
            .caseInsensitiveMatch = true;
    }]);
})();